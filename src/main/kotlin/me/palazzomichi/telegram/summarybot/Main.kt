package me.palazzomichi.telegram.summarybot

import io.github.ageofwar.telejam.Bot
import sun.misc.FpUtils
import java.nio.file.Files.createDirectories
import java.nio.file.Files.createFile
import java.nio.file.NoSuchFileException
import java.nio.file.Path
import java.nio.file.Paths
import java.util.*

private val BOT_PROPERTIES_PATH = Paths.get(".", "bot.properties")

fun main() {
    try {
        val (token, chatId, regexesPath, stringsPath, maxSummarySize) = loadBotProperties(BOT_PROPERTIES_PATH)
        val bot = Bot.fromToken(token)
        val chat = bot.getChat(chatId)
        val topics = TreeSet<Topic> { t1, t2 ->
            when {
                t1.message.date > t2.message.date -> 1
                t1.message.date < t2.message.date -> -1
                else -> t1.message.text.hashtags.indexOf(t1.name) -
                        t2.message.text.hashtags.indexOf(t2.name)
            }
        }.withMaxSize(maxSummarySize)
        val regexes = tryLoadRegexes(regexesPath).map(Regex::ignoreCase).toMutableSet()
        val strings = tryLoadStrings(stringsPath)
        SummaryBot(bot, chat, topics, regexes, regexesPath, strings).use {
            it.run()
        }
    } catch (e: NoSuchFileException) {
        BotProperties(
                token = "123456:ABC-DEF1234ghIkl-zyx57W2v1u123ew11",
                chatId = -123456789L,
                regexesPath = Paths.get(".", "regexes.txt"),
                stringsPath = Paths.get(".", "strings.json"),
                maxSummarySize = 50
        ).save(BOT_PROPERTIES_PATH)
    }
}

fun tryLoadStrings(path: Path) = try {
    loadStrings(path)
} catch (e: NoSuchFileException) {
    Strings().also {
        it.save(path)
    }
}

fun tryLoadRegexes(path: Path) = try {
    loadRegexes(path)
} catch (e: NoSuchFileException) {
    setOf<Regex>().also {
        createDirectories(path.parent)
        createFile(path)
    }
}

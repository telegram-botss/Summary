package me.palazzomichi.telegram.summarybot

import io.github.ageofwar.telejam.Bot
import io.github.ageofwar.telejam.callbacks.CallbackQuery
import io.github.ageofwar.telejam.chats.Chat
import io.github.ageofwar.telejam.connection.UploadFile
import io.github.ageofwar.telejam.messages.DocumentMessage
import io.github.ageofwar.telejam.messages.Message
import io.github.ageofwar.telejam.messages.TextMessage
import io.github.ageofwar.telejam.methods.*
import io.github.ageofwar.telejam.replymarkups.ReplyMarkup
import io.github.ageofwar.telejam.text.Text
import io.github.ageofwar.telejam.users.ChatMember
import io.github.ageofwar.telejam.users.User

fun Bot.getChat(chatId: Long): Chat {
    val getChat = GetChat()
            .chat(chatId)
    return execute(getChat)
}

fun Bot.sendMessage(replyToMessage: Message, text: Text, replyMarkup: ReplyMarkup? = null): TextMessage {
    val sendMessage = SendMessage()
            .replyToMessage(replyToMessage)
            .text(text)
            .replyMarkup(replyMarkup)
    return execute(sendMessage)
}

fun Bot.getChatMember(chat: Chat, user: User): ChatMember {
    val getChatMember = GetChatMember()
            .chat(chat)
            .user(user)
    return execute(getChatMember)
}

fun Bot.getChatMember(chat: Chat, userId: Long): ChatMember {
    val getChatMember = GetChatMember()
            .chat(chat)
            .user(userId)
    return execute(getChatMember)
}

fun Bot.sendDocument(replyToMessage: Message, document: UploadFile): DocumentMessage {
    val sendDocument = SendDocument()
            .replyToMessage(replyToMessage)
            .document(document)
    return execute(sendDocument)
}

fun Bot.canDeleteMessages(chat: Chat) = getChatMember(chat, id).canDeleteMessages()

fun Bot.deleteMessage(message: Message) {
    val deleteMessage = DeleteMessage()
            .message(message)
    execute(deleteMessage)
}

fun Bot.answerCallbackQuery(callbackQuery: CallbackQuery, text: String) {
    val answerCallbackQuery = AnswerCallbackQuery()
            .callbackQuery(callbackQuery)
            .text(text)
    execute(answerCallbackQuery)
}

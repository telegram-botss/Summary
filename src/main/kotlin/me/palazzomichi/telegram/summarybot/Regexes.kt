package me.palazzomichi.telegram.summarybot

import java.nio.charset.StandardCharsets.UTF_8
import java.nio.file.Files.*
import java.nio.file.Path
import java.util.stream.Collectors

typealias Regexes = Set<Regex>
typealias MutableRegexes = MutableSet<Regex>

fun loadRegexes(path: Path): Regexes = lines(path).map { Regex(it) }.collect(Collectors.toSet())

fun Regexes.save(path: Path) {
    createDirectories(path.parent)
    write(path, map { it.toString() }, UTF_8)
}

fun Regex.ignoreCase() = Regex(pattern, options + RegexOption.IGNORE_CASE)

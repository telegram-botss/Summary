package me.palazzomichi.telegram.summarybot

import io.github.ageofwar.telejam.inline.CallbackDataInlineKeyboardButton
import io.github.ageofwar.telejam.json.Json.fromJson
import io.github.ageofwar.telejam.json.Json.toPrettyJson
import io.github.ageofwar.telejam.replymarkups.InlineKeyboardMarkup
import java.nio.charset.StandardCharsets.UTF_8
import java.nio.file.Files.*
import java.nio.file.Path

data class Strings(
        val errors: Errors = Errors(),
        val commands: Commands = Commands(),
        val callbacks: Callbacks = Callbacks()
) {
    data class Errors(
            val noPermission: String = "<b>You have no permission to perform this command.</b>"
    )
    data class Commands(
            val help: Help = Help(),
            val summary: Summary = Summary(),
            val regexes: Regexes = Regexes(),
            val addRegexes: AddRegexes = AddRegexes(),
            val removeRegexes: RemoveRegexes = RemoveRegexes(),
            val addTopic: AddTopic = AddTopic(),
            val removeTopic: RemoveTopic = RemoveTopic()
    ) {
        data class Help(
                val name: String = "help",
                val aliases: List<String> = listOf("start"),
                val help: String = """
                    This bot gives you a list of discussed topics when you were offline.
                    #hashtags are used to define the topics.
                    Available commands:
                    - /summary sends the list of discussed topics after your latest message sent or after the quoted message
                """.trimIndent(),
                val adminHelp: String = """
                    This bot gives you a list of discussed topics when you were offline.
                    #hashtags are used to define the topics.
                    Regular expressions are used to filter undesired hashtags.
                    Available commands:
                    - /summary sends the list of discussed topics after your latest message sent or after the quoted message
                    - /regexes sends all saved regexes
                    - /addregexes &lt;regex&gt; [\n regex]... adds one or more regex filter
                    - /removeregexes &lt;regex&gt; [\n regex]... removes one or more saved regexes
                    - /addtopic &lt;topic&gt; adds a topic bypassing regex filter
                    - /removetopic &lt;topic&gt; removes one saved topic
                    - /removetopic removes all quoted topics
                """.trimIndent()
        )
        data class Summary(
                val name: String = "summary",
                val aliases: List<String> = emptyList(),
                val success: String = "<b>{0, choice, 0#No| 1#'{0}'} topic{0, choice, 0#| 1#| 2#s} found for</b> <a href=\"tg://user?id={2,number,###}\">{1}</a>{0, choice, 0#'<b>.</b>'| 1#'<b>:</b> {3}<b>.</b>'}",
                val keyboard: InlineKeyboardMarkup = InlineKeyboardMarkup(CallbackDataInlineKeyboardButton("delete", "delete_with_reply"))
        )
        data class Regexes(
                val name: String = "regexes",
                val aliases: List<String> = emptyList(),
                val noRegex: String = "<b>There are no regex saved yet!</b>"
        )
        data class AddRegexes(
                val name: String = "addregexes",
                val aliases: List<String> = listOf("addregex"),
                val syntax: String = "<b>Syntax:</b> <code>/addregexes &lt;regex&gt; [\\n regex]...</code>",
                val success: String = "<b>{0} regex{0, choice, 1#| 2#s} added.</b>",
                val wrongSyntax: String = "<b>Wrong syntax:</b> {0}"
        )
        data class RemoveRegexes(
                val name: String = "removeregexes",
                val aliases: List<String> = listOf("removeregex"),
                val syntax: String = "<b>Syntax:</b> <code>/removeregexes &lt;regex&gt; [\\n regex]...</code>",
                val success: String = "<b>{0} regex{0, choice, 0#s| 1#| 2#s} removed.{1, choice, 0#| 1#'\n{1} regex{1, choice, 1#| 2#s} not found.'}</b>"
        )
        data class AddTopic(
                val name: String = "addtopic",
                val aliases: List<String> = emptyList(),
                val syntax: String = "<b>Syntax:</b> <code>/addtopic &lt;topic&gt;</code>",
                val success: String = "#{0}"
        )
        data class RemoveTopic(
                val name: String = "removetopic",
                val aliases: List<String> = emptyList(),
                val syntax: String = "<b>Syntax:</b> <code>/removetopic &lt;topic&gt;</code>",
                val success: String = "<b>{0, choice, 0#No topic found.| 1#Topic(s) removed.}</b>"
        )
    }
    data class Callbacks(
            val deleteWithReply: DeleteWithReply = DeleteWithReply()
    ) {
        data class DeleteWithReply(
                val name: String = "delete_with_reply",
                val wrongUser: String = "Only the {0} can do this."
        )
    }
}

fun loadStrings(path: Path): Strings {
    return fromJson(newBufferedReader(path), Strings::class.java)
}

fun Strings.save(path: Path) {
    createDirectories(path.parent)
    newBufferedWriter(path, UTF_8).use {
        toPrettyJson(this, it)
    }
}

package me.palazzomichi.telegram.summarybot

import io.github.ageofwar.telejam.Bot
import io.github.ageofwar.telejam.LongPollingBot
import io.github.ageofwar.telejam.callbacks.CallbackDataHandler
import io.github.ageofwar.telejam.callbacks.CallbackQuery
import io.github.ageofwar.telejam.chats.Chat
import io.github.ageofwar.telejam.commands.Command
import io.github.ageofwar.telejam.commands.CommandHandler
import io.github.ageofwar.telejam.connection.UploadFile
import io.github.ageofwar.telejam.messages.*
import io.github.ageofwar.telejam.text.Text
import io.github.ageofwar.telejam.updates.Update
import java.io.FileNotFoundException
import java.nio.file.Path
import java.text.MessageFormat.format
import java.util.function.Predicate
import java.util.regex.PatternSyntaxException

class SummaryBot(
        bot: Bot,
        chat: Chat,
        topics: MutableSet<Topic>,
        regexes: MutableRegexes,
        regexesPath: Path,
        strings: Strings
) : LongPollingBot(bot) {
    private val topicFilter = { topic: Topic, topics: Set<Topic>, _: Message ->
        RegexFilter(regexes).test(topic.name) && topics.lastOrNull()?.name != topic.name
    }
    private val topicCollector = TopicCollector(chat, topics, topicFilter)
    private val lastAccessCollector = LastAccessCollector(chat, mutableMapOf())
    private val deleteMessageCallback = DeleteWithReplyMessageCallback(bot, strings.callbacks.deleteWithReply)

    init {
        events.apply {
            val (errors, commands, callbacks) = strings
            val lastAccesses = lastAccessCollector.lastAccesses
            val errorMessage = Text.parseHtml(errors.noPermission)

            registerHelpCommand(bot, chat, commands.help)
            registerSummaryCommand(bot, topics, lastAccesses, commands.summary)
            registerRegexesCommand(bot, chat, regexesPath, commands.regexes, errorMessage)
            registerAddRegexesCommand(bot, chat, regexes, regexesPath, commands.addRegexes, errorMessage)
            registerRemoveRegexesCommand(bot, chat, regexes, regexesPath, commands.removeRegexes, errorMessage)
            registerAddTopicCommand(bot, chat, topics, commands.addTopic, errorMessage)
            registerRemoveTopicCommand(bot, chat, topics, commands.removeTopic, errorMessage)

            registerUpdateHandler(topicCollector)
            registerUpdateHandler(lastAccessCollector)

            registerCallbackDataHandler(deleteMessageCallback, callbacks.deleteWithReply.name)
        }
    }
}

class HelpCommand(
        private val bot: Bot,
        private val chat: Chat,
        private val strings: Strings.Commands.Help
) : CommandHandler {
    override fun onCommand(command: Command, message: TextMessage) {
        val member = bot.getChatMember(chat, message.sender)
        if (member.isAdmin) {
            bot.sendMessage(message, Text.parseHtml(strings.adminHelp))
        } else {
            bot.sendMessage(message, Text.parseHtml(strings.help))
        }
    }
}

data class Topic(val name: String, val message: TextMessage)

class TopicCollector(
        private val chat: Chat,
        private val topics: MutableSet<Topic>,
        private val topicFilter: (Topic, Set<Topic>, Message) -> Boolean
) : TextMessageHandler, MessageEditHandler {
    override fun onTextMessage(message: TextMessage) {
        if (message.chat == chat) {
            val text = message.text
            topics += text.hashtags.toSet().map {
                Topic(it, message)
            }.filter {
                topicFilter(it, topics, message)
            }
        }
    }

    override fun onMessageEdit(message: Message, editDate: Long) {
        if (message.chat == chat && message is TextMessage) {
            topics.removeIf { message == it.message }
            onMessage(message)
        }
    }

    override fun onUpdate(update: Update) {
        super<TextMessageHandler>.onUpdate(update)
        super<MessageEditHandler>.onUpdate(update)
    }
}

class RegexFilter(private val regexes: Regexes): Predicate<String> {
    override fun test(s: String): Boolean {
        for (regex in regexes) {
            if (regex.matches(s)) {
                return false
            }
        }
        return true
    }
}

class LastAccessCollector(
        private val chat: Chat,
        val lastAccesses: MutableMap<Long, Long>
) : MessageHandler {
    override fun onMessage(message: Message) {
        if (message.chat == chat) {
            lastAccesses[message.sender.id] = message.date
        }
    }
}

class SummaryCommand(
        private val bot: Bot,
        private val topics: Set<Topic>,
        private val lastAccesses: Map<Long, Long>,
        private val strings: Strings.Commands.Summary
) : CommandHandler {
    override fun onCommand(command: Command, message: TextMessage) {
        val fromDate = message.replyToMessage.map { it.date }.orElseGet {
            lastAccesses[message.sender.id] ?: 0
        }
        val topics = topics.filter {
            (_, message) -> fromDate < message.date
        }.map { Text.link(it.name, it.message).toHtmlString() }
        val text = format(
                strings.success, topics.size, message.sender.name, message.sender.id,
                topics.joinToString(", ")
        )
        val keyboard = if (bot.canDeleteMessages(message.chat)) strings.keyboard else null
        bot.sendMessage(message, Text.parseHtml(text), keyboard)
    }
}

class DeleteWithReplyMessageCallback(
        private val bot: Bot,
        private val strings: Strings.Callbacks.DeleteWithReply
) : CallbackDataHandler {
    override fun onCallbackData(callbackQuery: CallbackQuery, name: String, args: String) {
        callbackQuery.message.ifPresent {
            val sender = callbackQuery.message.get().replyToMessage.map(Message::getSender)
            if (!sender.isPresent || callbackQuery.sender == sender.get()) {
                bot.deleteMessage(it)
                it.replyToMessage.ifPresent(bot::deleteMessage)
            } else {
                val text = format(strings.wrongUser, sender.get().name)
                bot.answerCallbackQuery(callbackQuery, text)
            }
        }
    }
}

class RegexesCommand(
        private val bot: Bot,
        private val regexesPath: Path,
        private val strings: Strings.Commands.Regexes
) : CommandHandler {
    override fun onCommand(comand: Command, message: TextMessage) {
        try {
            bot.sendDocument(message, UploadFile.fromFile(regexesPath.toString()))
        } catch (e: FileNotFoundException) {
            bot.sendMessage(message, Text.parseHtml(strings.noRegex))
        }
    }
}

class AddRegexesCommand(
        private val bot: Bot,
        private val regexes: MutableRegexes,
        private val regexesPath: Path,
        private val strings: Strings.Commands.AddRegexes
) : CommandHandler {
    override fun onCommand(command: Command, message: TextMessage) {
        val args = command.args
        if (args.isEmpty) {
            bot.sendMessage(message, Text.parseHtml(strings.syntax))
            return
        }
        try {
            val lines = args.lines()
            regexes += lines.map { Regex(it, RegexOption.IGNORE_CASE) }
            regexes.save(regexesPath)
            val text = format(strings.success, lines.size)
            bot.sendMessage(message, Text.parseHtml(text))
        } catch (e: PatternSyntaxException) {
            val text = format(strings.wrongSyntax, e.message)
            bot.sendMessage(message, Text.parseHtml(text))
        }
    }
}

class RemoveRegexesCommand(
        private val bot: Bot,
        private val regexes: MutableRegexes,
        private val regexesPath: Path,
        private val strings: Strings.Commands.RemoveRegexes
) : CommandHandler {
    override fun onCommand(command: Command, message: TextMessage) {
        val args = command.args
        if (args.isEmpty) {
            bot.sendMessage(message, Text.parseHtml(strings.syntax))
            return
        }
        var removed = 0
        var notFound = 0
        args.lineSequence().forEach {
            try {
                if (regexes.removeIf { regex -> regex.toString() == it }) {
                    removed++
                } else {
                    notFound++
                }
            } catch (e: PatternSyntaxException) {
                notFound++
            }
        }
        regexes.save(regexesPath)
        val text = format(strings.success, removed, notFound)
        bot.sendMessage(message, Text.parseHtml(text))
    }
}

class AddTopicCommand(
        private val bot: Bot,
        private val topics: MutableSet<Topic>,
        private val strings: Strings.Commands.AddTopic
) : CommandHandler {
    override fun onCommand(command: Command, message: TextMessage) {
        val args = command.args.toString()
        if (args.isEmpty() || !args.matches(Regex("\\w+"))) {
            bot.sendMessage(message, Text.parseHtml(strings.syntax))
            return
        }
        topics += Topic(args, message)
        val text = format(strings.success, args)
        bot.sendMessage(message, Text.parseHtml(text))
    }
}

class RemoveTopicCommand(
        private val bot: Bot,
        private val topics: MutableSet<Topic>,
        private val strings: Strings.Commands.RemoveTopic
) : CommandHandler {
    override fun onCommand(command: Command, message: TextMessage) {
        val args = command.args.toString()
        if (args.isEmpty() && !message.replyToMessage.isPresent) {
            bot.sendMessage(message, Text.parseHtml(strings.syntax))
            return
        }
        val success = when {
            args.isNotEmpty() -> topics.removeIf { it.name.equals(args, ignoreCase = true) }
            message.replyToMessage.get() is TextMessage -> {
                val replyToMessage = message.replyToMessage.get() as TextMessage
                val toRemove = replyToMessage.text.hashtags.map { Topic(it, replyToMessage) }
                topics.removeAll(toRemove)
            }
            else -> false
        }
        val text = format(strings.success, if (success) 1 else 0 )
        bot.sendMessage(message, Text.parseHtml(text))
    }
}

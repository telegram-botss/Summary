package me.palazzomichi.telegram.summarybot

import java.util.*

fun <T> NavigableSet<T>.withMaxSize(size: Int) = object : NavigableSet<T> by this {
    override fun add(element: T): Boolean {
        checkSize(1)
        return this@withMaxSize.add(element)
    }

    override fun addAll(elements: Collection<T>): Boolean {
        checkSize(elements.size)
        return this@withMaxSize.addAll(elements)
    }

    private fun checkSize(toAdd: Int) {
        var toRemove = this.size + toAdd - size
        while (toRemove > 0) {
            pollFirst()
            toRemove--
        }
    }
}
package me.palazzomichi.telegram.summarybot

import io.github.ageofwar.telejam.Bot
import io.github.ageofwar.telejam.chats.Chat
import io.github.ageofwar.telejam.commands.CommandHandler
import io.github.ageofwar.telejam.events.EventRegistry
import io.github.ageofwar.telejam.text.Text
import java.nio.file.Path

fun EventRegistry.registerHelpCommand(
        bot: Bot,
        chat: Chat,
        strings: Strings.Commands.Help
) {
    registerCommand(
            HelpCommand(bot, chat, strings),
            strings.name, *strings.aliases.toTypedArray()
    )
}

fun EventRegistry.registerSummaryCommand(
        bot: Bot,
        topics: Set<Topic>,
        lastAccesses: Map<Long, Long>,
        strings: Strings.Commands.Summary) {
    registerCommand(
            SummaryCommand(bot, topics, lastAccesses, strings),
            strings.name, *strings.aliases.toTypedArray()
    )
}

fun EventRegistry.registerRegexesCommand(
        bot: Bot,
        chat: Chat,
        regexesPath: Path,
        strings: Strings.Commands.Regexes,
        errorMessage: Text
) {
    registerCommand(
            RegexesCommand(bot, regexesPath, strings).adminCommand(chat, bot, errorMessage),
            strings.name, *strings.aliases.toTypedArray()
    )
}

fun EventRegistry.registerAddRegexesCommand(
        bot: Bot,
        chat: Chat,
        regexes: MutableRegexes,
        regexesPath: Path,
        strings: Strings.Commands.AddRegexes,
        errorMessage: Text
) {
    registerCommand(
            AddRegexesCommand(bot, regexes, regexesPath, strings).adminCommand(chat, bot, errorMessage),
            strings.name, *strings.aliases.toTypedArray()
    )
}

fun EventRegistry.registerRemoveRegexesCommand(
        bot: Bot,
        chat: Chat,
        regexes: MutableRegexes,
        regexesPath: Path,
        strings: Strings.Commands.RemoveRegexes,
        errorMessage: Text
) {
    registerCommand(
            RemoveRegexesCommand(bot, regexes, regexesPath, strings).adminCommand(chat, bot, errorMessage),
            strings.name, *strings.aliases.toTypedArray()
    )
}

fun EventRegistry.registerAddTopicCommand(
        bot: Bot,
        chat: Chat,
        topics: MutableSet<Topic>,
        strings: Strings.Commands.AddTopic,
        errorMessage: Text
) {
    registerCommand(
            AddTopicCommand(bot, topics, strings).adminCommand(chat, bot, errorMessage),
            strings.name, *strings.aliases.toTypedArray()
    )
}

fun EventRegistry.registerRemoveTopicCommand(
        bot: Bot,
        chat: Chat,
        topics: MutableSet<Topic>,
        strings: Strings.Commands.RemoveTopic,
        errorMessage: Text
) {
    registerCommand(
            RemoveTopicCommand(bot, topics, strings).adminCommand(chat, bot, errorMessage),
            strings.name, *strings.aliases.toTypedArray()
    )
}

fun CommandHandler.adminCommand(chat: Chat, bot: Bot, errorMessage: Text): CommandHandler {
    return CommandHandler { command, message ->
        val member = bot.getChatMember(chat, message.sender)
        if (member.isAdmin) {
            this.onCommand(command, message)
        } else {
            bot.sendMessage(message, errorMessage)
        }
    }
}

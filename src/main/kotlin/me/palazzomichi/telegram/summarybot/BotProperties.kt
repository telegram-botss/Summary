package me.palazzomichi.telegram.summarybot

import java.nio.file.Files.*
import java.nio.file.Path
import java.nio.file.Paths
import java.util.*

data class BotProperties(
        val token: String,
        val chatId: Long,
        val regexesPath: Path,
        val stringsPath: Path,
        val maxSummarySize: Int
)

fun loadBotProperties(path: Path): BotProperties {
    val botProperties = Properties().apply {
        load(newInputStream(path))
    }
    val token = botProperties.getNotNullProperty("token")
    val chatId = botProperties.getNotNullProperty("chat-id").toLong()
    val regexesPath = botProperties.getNotNullProperty("regexes-path")
    val stringsPath = botProperties.getNotNullProperty("strings-path")
    val maxSummarySize = botProperties.getNotNullProperty("max-summary-size").toInt()
    return BotProperties(token, chatId, Paths.get(regexesPath), Paths.get(stringsPath), maxSummarySize)
}

private fun Properties.getNotNullProperty(property: String) = getProperty(property)
        ?: throw MissingPropertyException(property)

fun BotProperties.save(path: Path) {
    createDirectories(path.parent)
    val botProperties = Properties().apply {
        setProperty("token", token)
        setProperty("chat-id", chatId.toString())
        setProperty("regexes-path", regexesPath.toString())
        setProperty("strings-path", stringsPath.toString())
        setProperty("max-summary-size", maxSummarySize.toString())
    }
    newOutputStream(path).use { botProperties.store(it, null) }
}

class MissingPropertyException(property: String) : Exception("cannot find property '$property'")
